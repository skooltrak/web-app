import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslocoModule } from '@ngneat/transloco';
import { NgxSummernoteModule } from 'ngx-summernote';

import { AnnouncementsComponent } from './announcements.component';
import { AnnouncementsRoutingModule } from './announcements.routes';
import { NewAnnouncementComponent } from './new-announcement/new-announcement.component';
import { CustomTableComponent } from 'src/app/shared/components/custom-components/custom-table/custom-table.component';

@NgModule({
  declarations: [AnnouncementsComponent, NewAnnouncementComponent],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    AnnouncementsRoutingModule,
    CustomTableComponent,
    NgxSummernoteModule,
    TranslocoModule,
  ],
})
export class AnnouncementsModule {}
