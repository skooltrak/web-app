import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslocoModule } from '@ngneat/transloco';

import { DetailsComponent } from './details/details.component';
import { TeachersComponent } from './teachers.component';
import { TeachersRoutingModule } from './teachers.routes';
import { CustomTableComponent } from 'src/app/shared/components/custom-components/custom-table/custom-table.component';
import { LoadingModalComponent } from 'src/app/shared/components/custom-components/loading-modal/loading-modal.component';

@NgModule({
  declarations: [TeachersComponent, DetailsComponent],
  imports: [
    CommonModule,
    TranslocoModule,
    CustomTableComponent,
    LoadingModalComponent,
    TeachersRoutingModule,
    NgbModule,
  ],
})
export class TeachersModule {}
