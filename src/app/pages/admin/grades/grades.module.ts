import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbNavModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslocoModule } from '@ngneat/transloco';

import { CreditsComponent } from './credits/credits.component';
import { GradesComponent } from './grades.component';
import { GradesRoutingModule } from './grades.routes';
import { RankingsComponent } from './rankings/rankings.component';
import { CustomSelectComponent } from 'src/app/shared/components/custom-components/custom-select/custom-select.component';

@NgModule({
  declarations: [GradesComponent, CreditsComponent, RankingsComponent],
  imports: [
    CommonModule,
    TranslocoModule,
    FormsModule,
    GradesRoutingModule,
    CustomSelectComponent,
    NgbNavModule,
  ],
})
export class GradesModule {}
