import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslocoModule } from '@ngneat/transloco';

import { ContactFormComponent } from './contact-form/contact-form.component';
import { SchoolsEditComponent } from './schools-edit/schools-edit.component';
import { SchoolsFormComponent } from './schools-form/schools-form.component';
import { SchoolsNewComponent } from './schools-new/schools-new.component';
import { SchoolsRoutingModule } from './schools-routing.module';
import { SchoolsComponent } from './schools.component';
import { CustomTableComponent } from 'src/app/shared/components/custom-components/custom-table/custom-table.component';
import { LoadingModalComponent } from 'src/app/shared/components/custom-components/loading-modal/loading-modal.component';

@NgModule({
  declarations: [
    SchoolsComponent,
    SchoolsNewComponent,
    SchoolsEditComponent,
    SchoolsFormComponent,
    ContactFormComponent,
  ],
  imports: [
    CommonModule,
    SchoolsRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    CustomTableComponent,
    LoadingModalComponent,
    TranslocoModule,
  ],
})
export class SchoolsModule {}
