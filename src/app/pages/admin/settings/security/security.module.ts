import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslocoModule } from '@ngneat/transloco';

import { SecurityRoutingModule } from './security-routing.module';
import { UsersComponent } from './users/users.component';
import { CustomTableComponent } from 'src/app/shared/components/custom-components/custom-table/custom-table.component';
import { LoadingModalComponent } from 'src/app/shared/components/custom-components/loading-modal/loading-modal.component';
import { CustomSelectComponent } from 'src/app/shared/components/custom-components/custom-select/custom-select.component';

@NgModule({
  declarations: [UsersComponent],
  imports: [
    CommonModule,
    TranslocoModule,
    SecurityRoutingModule,
    CustomTableComponent,
    LoadingModalComponent,
    CustomSelectComponent,
  ],
})
export class SecurityModule {}
