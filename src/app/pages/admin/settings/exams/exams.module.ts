import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgbNavModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslocoModule } from '@ngneat/transloco';
import { NgxSummernoteModule } from 'ngx-summernote';

import { DetailsComponent } from './details/details.component';
import { ExamsRoutingModule } from './exams-routing.module';
import { ExamsComponent } from './exams.component';
import { PreviewMatchComponent } from './preview-match/preview-match.component';
import { PreviewComponent } from './preview/preview.component';
import { CustomTableComponent } from 'src/app/shared/components/custom-components/custom-table/custom-table.component';
import { LoadingModalComponent } from 'src/app/shared/components/custom-components/loading-modal/loading-modal.component';

@NgModule({
  declarations: [
    ExamsComponent,
    DetailsComponent,
    PreviewComponent,
    PreviewMatchComponent,
  ],
  imports: [
    CommonModule,
    ExamsRoutingModule,
    TranslocoModule,
    CustomTableComponent,
    NgbNavModule,
    LoadingModalComponent,
    DragDropModule,
    NgxSummernoteModule,
  ],
})
export class ExamsModule {}
