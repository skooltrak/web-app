import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslocoModule } from '@ngneat/transloco';

import { NgxSummernoteModule } from 'ngx-summernote';

import { DetailsComponent } from './details/details.component';
import { FormComponent } from './form/form.component';
import { IncidentsRoutingModule } from './incidents-routing.module';
import { IncidentsComponent } from './incidents.component';
import { NewComponent } from './new/new.component';
import { CustomTableComponent } from 'src/app/shared/components/custom-components/custom-table/custom-table.component';
import { CustomSelectComponent } from 'src/app/shared/components/custom-components/custom-select/custom-select.component';

@NgModule({
  declarations: [
    IncidentsComponent,
    DetailsComponent,
    NewComponent,
    FormComponent,
  ],
  imports: [
    CommonModule,
    IncidentsRoutingModule,
    CustomTableComponent,
    CustomSelectComponent,
    TranslocoModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSummernoteModule,
  ],
})
export class IncidentsModule {}
