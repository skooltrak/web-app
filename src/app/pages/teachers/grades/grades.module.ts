import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbNavModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslocoModule } from '@ngneat/transloco';
import { LoadingModalComponent } from 'src/app/shared/components/custom-components/loading-modal/loading-modal.component';

import { FinalScoreComponent } from './final-score/final-score.component';
import { GradesRoutingModule } from './grades-routing.module';
import { GradesComponent } from './grades.component';
import { PeriodGradesComponent } from './period-grades/period-grades.component';

@NgModule({
  declarations: [GradesComponent, PeriodGradesComponent, FinalScoreComponent],
  imports: [
    CommonModule,
    GradesRoutingModule,
    TranslocoModule,
    NgbNavModule,
    LoadingModalComponent,
    FormsModule,
  ],
})
export class GradesModule {}
