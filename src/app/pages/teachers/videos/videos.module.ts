import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslocoModule } from '@ngneat/transloco';

import { VideoPlayerModule } from 'src/app/shared/components/video-player/video-player.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { VideosRoutingModule } from './videos-routing.module';
import { VideosComponent } from './videos.component';
import { LoadingModalComponent } from 'src/app/shared/components/custom-components/loading-modal/loading-modal.component';

@NgModule({
  declarations: [VideosComponent],
  imports: [
    CommonModule,
    VideosRoutingModule,
    NgbModalModule,
    TranslocoModule,
    VideoPlayerModule,
    LoadingModalComponent,
    SharedModule,
  ],
})
export class VideosModule {}
