import { WeekDay } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CalendarEvent, CalendarView, DAYS_OF_WEEK } from 'angular-calendar';
import { add, addDays, endOfWeek, format, formatDistance, isSameDay, isSameMonth, isSunday, startOfWeek } from 'date-fns';
import { es } from 'date-fns/locale';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AssignmentDetailsComponent } from 'src/app/shared/components/assignment-details/assignment-details.component';
import { Activity } from 'src/app/shared/models/activities.model';
import { Assignment, AssignmentsDay } from 'src/app/shared/models/assignments.model';
import { AssignmentService } from 'src/app/shared/services/assignments.service';
import { CoursesService } from 'src/app/shared/services/courses.service';
import { SessionService } from 'src/app/shared/services/session.service';
import { StudentsService } from 'src/app/shared/services/students.service';

@Component({
  selector: 'skooltrak-assignments',
  templateUrl: './assignments.component.html',
  styleUrls: ['./assignments.component.sass'],
})
export class AssignmentsComponent implements OnInit {
  view: CalendarView = CalendarView.Week;
  CalendarView = CalendarView;

  viewDate: Date = new Date();
  assignment$: Observable<CalendarEvent<{ assignment: Assignment }>[]>;
  activeDayIsOpen = false;
  assignments$: Observable<Assignment[]>;
  isLoading = false;
  mapped: AssignmentsDay[];
  selected: Assignment;
  excludeDays: number[] = [0, 6];
  weekStart: Date;
  weekEnd: Date;
  activities$: Observable<Activity[]>;
  weekStartsOn = DAYS_OF_WEEK.MONDAY;

  constructor(
    private studentsService: StudentsService,
    private router: Router,
    private route: ActivatedRoute,
    private assignmentService: AssignmentService,
    private session: SessionService,
    private modal: NgbModal,
    public coursesService: CoursesService
  ) {}

  ngOnInit(): void {
    this.fetchEvents();
    this.activities$ = this.studentsService.getActivities(
      this.session.currentStudent?.id
    );
    this.weekStart = startOfWeek(new Date(), { weekStartsOn: 1 });
    this.weekEnd = addDays(this.weekStart, 6);
  }

  mapWeek() {
    this.isLoading = true;
    this.weekStart = startOfWeek(this.viewDate, {
      weekStartsOn: WeekDay.Monday,
    });
    this.weekEnd = endOfWeek(this.viewDate, { weekStartsOn: WeekDay.Monday });
    this.assignments$.subscribe({
      next: (res) => {
        this.mapped = this.assignmentService.mapAssignments(
          this.weekStart,
          this.weekEnd,
          res
        );
        this.isLoading = false;
      },
      error: (err) => console.error(err),
    });
  }

  fetchEvents(): void {
    this.assignments$ = this.studentsService.getAssignments(
      this.session.currentUser.people[0].id,
      this.weekStart,
      this.weekEnd
    );
    this.mapWeek();
    this.assignment$ = this.studentsService
      .getAssignments(
        this.session.currentUser.people[0].id,
        this.weekStart,
        this.weekEnd
      )
      .pipe(
        map((res) =>
          res.map((assignment) => ({
            id: assignment.id,
            title: assignment.title,
            allDay: true,
            start: add(new Date(assignment.startDate), { hours: 6 }),
            end: add(new Date(assignment.dueDate), { hours: 12 }),
            meta: {
              assignment,
            },
          }))
        )
      );
  }

  dayClicked({
    date,
    events,
  }: {
    date: Date;
    events: CalendarEvent<{ assignment: Assignment }>[];
  }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
        this.viewDate = date;
      }
    }
  }

  hideSundays(day: AssignmentsDay) {
    return isSunday(day.date);
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  formatDate(date: Date) {
    return format(date, 'iiii d', { locale: es });
  }

  closeOpenMonthViewDay() {
    this.mapWeek();
    this.activeDayIsOpen = false;
  }

  openDetails(assignment: Assignment) {
    const modalRef = this.modal.open(AssignmentDetailsComponent, {
      size: 'lg',
      centered: true,
    });
    modalRef.result.then((result) => {
      if (result === 'course') {
        this.router.navigate(['../../courses', assignment.course.id], {
          relativeTo: this.route,
        });
      } else {
        this.router.navigate([assignment.id], { relativeTo: this.route });
      }
    });
    modalRef.componentInstance.assignment = assignment;
  }

  selectDay(event: CalendarEvent) {
    this.router.navigate([event.meta.assignment.id], {
      relativeTo: this.route,
    });
  }

  formatDue(date: Date) {
    return formatDistance(new Date(), new Date(date), {
      locale: es,
    });
  }

  getValues() {
    const array: string[][] = [];
    return array;
  }
}
