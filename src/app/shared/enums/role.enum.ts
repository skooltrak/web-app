export const enum RoleType {
  Administrator = 1,
  Teacher = 2,
  Student = 3,
  Parent = 4
}
