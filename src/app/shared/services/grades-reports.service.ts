import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { format } from 'date-fns';
import { es } from 'date-fns/locale';
import { mean } from 'lodash';
import { firstValueFrom } from 'rxjs';
import { environment } from 'src/environments/environment';

import { Period } from '../models/periods.model';
import { StudentSkill } from '../models/skills.model';
import { GradeSummary } from '../models/students.model';
import { FilesService } from './files.service';
import { SessionService } from './session.service';
import { StudentsService } from './students.service';
import { StudyPlanService } from './study-plans.service';

@Injectable({ providedIn: 'root' })
export class GradesReportsService {
  background: any;
  currentScore: number;
  constructor(
    private studentService: StudentsService,
    private filesService: FilesService,
    private planService: StudyPlanService,
    private session: SessionService,
    private http: HttpClient
  ) {
    this.http
      .get('/assets/img/report-background.jpg', { responseType: 'blob' })
      .subscribe({
        next: (res) => {
          const reader = new FileReader();
          reader.onloadend = () => {
            const base64data = reader.result;
            this.background = base64data;
          };

          reader.readAsDataURL(res);
        },
        error: (err) => console.error(err),
      });
  }

  async generatePDF(studentId: string, period: Period) {
    // const image = await this.filesService.getBase64ImageFromURL(
    //   this.schoolsService.getLogo(this.session.currentSchool)
    // );
    const student = await firstValueFrom(this.studentService.get(studentId));
    const courses = await firstValueFrom(
      this.studentService.getSummary(studentId, period)
    );
    const skills = await firstValueFrom(
      this.studentService.getSkills(studentId)
    );
    const plan = await firstValueFrom(this.planService.get(student.plan.id));
    this.currentScore = await firstValueFrom(
      this.studentService.getCurrentScore(studentId)
    );
    const header = {
      columns: [
        {
          text: '',
          margin: [20, 20],
          width: 175,
          fontSize: 7,
          alignment: 'right',
        },
        {
          stack: [
            'REPUBLICA DE PANAMÁ',
            'MINISTERIO DE EDUCACIÓN',
            this.session.currentSchool.name.toUpperCase(),
            '  ',
            'INFORME TRIMESTRAL',
            `AÑO LECTIVO ${environment.currentYear}`,
          ],
          alignment: 'center',
          bold: true,
          fontSize: 8.5,
        },
        {
          text: '',
          margin: [20, 20],
          width: 175,
          fontSize: 8,
          alignment: 'right',
        },
      ],
      margin: [20, 10, 20, 10],
    };

    const info = {
      author: this.session.currentSchool.name,
    };
    const date = new Date();
    const studentInfo = [
      {
        columns: [
          {
            text: [
              { text: 'ESTUDIANTE: ' },
              { text: student.fullName.toUpperCase(), color: 'white' },
            ],
            bold: true,
            fontSize: 8,
          },
          {
            text: [
              { text: 'CÉDULA: ', bold: true },
              { text: student.documentId, color: 'white' },
            ],
            fontSize: 8,
          },
        ],
        margin: [0, 5, 0, 0],
      },
      {
        columns: [
          {
            text: [
              { text: 'SECCIÓN: ', bold: true },
              { text: plan.degree?.name.toUpperCase() },
            ],
            fontSize: 8,
          },
          {
            columns: [
              {
                text: [
                  { text: 'GRADO: ', bold: true },
                  { text: student.group.level.name },
                ],
                fontSize: 8,
              },
              {
                text: [
                  { text: 'FECHA: ', bold: true },
                  {
                    text: format(date, "d 'de' MMMM 'de' yyyy", { locale: es }),
                  },
                ],
              },
            ],

            fontSize: 8,
            width: '*',
          },
        ],
        margin: [0, 0, 0, 5],
      },
    ];

    const coursesTable = {
      fontSize: 7,
      table: {
        headerRows: 3,
        body: this.getValues(courses),
        widths: [180, 25, 25, 25, 28, 20, 20, 20, 20, 20, 20, 20, 20],
      },
    };

    const skillTable = {
      fontSize: 7,
      table: {
        headerRows: 1,
        body: this.getSkills(skills),
        widths: [180, 114.5, 114.5, 114.5],
      },
      margin: [0, 10, 0, 0],
    };

    const systemInfo = {
      text: 'SISTEMA DE CALIFICACIÓN: 5.0 NOTA MÁXIMA, 3.0 NOTA MÍNINA PARA APROBAR EL AÑO, 1.0 NOTA MÍNIMA.',
      bold: true,
      fontSize: 7,
      margin: [0, 5, 0, 0],
    };

    const skillsInfo = {
      text: 'HÁBITOS Y ACTITUDES SE CALIFICARÁN ASÍ: (S): SATISFACTORIO, (R): REGULAR, (X): DEFICIENTE.',
      bold: true,
      fontSize: 7.5,
      margin: [0, 0],
    };
    const notes = {
      text: 'OBSERVACIONES:',
      bold: true,
      fontSize: 8,
      margin: [0, 5, 0, 0],
    };

    return {
      defaultStyle: { font: 'Roboto' },
      pageSize: 'LETTER',
      info,
      header,
      background: [
        {
          image: await this.filesService.getBase64ImageFromURL(this.background),
          width: 611,
          absolutePosition: { x: 0, y: 0 },
        },
      ],
      content: [
        studentInfo,
        coursesTable,
        skillTable,
        systemInfo,
        skillsInfo,
        notes,
        {
          canvas: [
            {
              type: 'line',
              x1: 0,
              y1: 5,
              x2: 557,
              y2: 5,
              lineWidth: 0.5,
            },
          ],
          margin: [0, 5],
        },
        {
          canvas: [
            {
              type: 'line',
              x1: 0,
              y1: 5,
              x2: 557,
              y2: 5,
              lineWidth: 0.5,
            },
          ],
          margin: [0, 5],
        },
        {
          canvas: [
            { type: 'line', x1: 0, y1: 5, x2: 200, y2: 5, lineWidth: 0.5 },
            { type: 'line', x1: 357, y1: 5, x2: 557, y2: 5, lineWidth: 0.5 },
          ],
          margin: [0, 20, 0, 2],
        },
        {
          columns: [
            {
              width: 200,
              fontSize: 9,
              text: 'Director',
              bold: true,
              alignment: 'center',
            },
            {
              width: 157,
              text: '',
              bold: true,
              alignment: 'center',
            },
            {
              width: 200,
              fontSize: 9,
              text: 'Consejería',
              bold: true,
              alignment: 'center',
            },
          ],
        },
      ],
      pageMargins: [20, 80, 20, 35],
    };
  }

  getValues(summary: GradeSummary) {
    const array: any[][] = [];
    array.push([
      { text: 'ASIGNATURAS', bold: true, rowSpan: 3 },
      { text: 'CALIFICACIONES', bold: true, alignment: 'center', colSpan: 4 },
      { text: '' },
      { text: '' },
      { text: '' },
      {
        text: 'AUSENCIAS Y TARDANZAS',
        bold: true,
        alignment: 'center',
        colSpan: 8,
      },
      { text: '' },
      { text: '' },
      { text: '' },
      { text: '' },
      { text: '' },
      { text: '' },
      { text: '' },
    ]);
    array.push([
      { text: '' },
      { text: 'I    TRIM', bold: true, alignment: 'center', rowSpan: 2 },
      { text: 'II   TRIM', bold: true, alignment: 'center', rowSpan: 2 },
      { text: 'III  TRIM', bold: true, alignment: 'center', rowSpan: 2 },
      { text: 'PROM FINAL', bold: true, alignment: 'center', rowSpan: 2 },
      { text: 'I TRIM', bold: true, alignment: 'center', colSpan: 2 },
      { text: '' },
      { text: 'II TRIM', bold: true, alignment: 'center', colSpan: 2 },
      { text: '' },
      { text: 'III TRIM', bold: true, alignment: 'center', colSpan: 2 },
      { text: '' },
      { text: 'TOTAL', bold: true, alignment: 'center', colSpan: 2 },
      { text: '' },
    ]);
    array.push([
      { text: '' },
      { text: '' },
      { text: '' },
      { text: '' },
      { text: '' },
      { text: 'A', bold: true, alignment: 'center' },
      { text: 'T', bold: true, alignment: 'center' },
      { text: 'A', bold: true, alignment: 'center' },
      { text: 'T', bold: true, alignment: 'center' },
      { text: 'A', bold: true, alignment: 'center' },
      { text: 'T', bold: true, alignment: 'center' },
      { text: 'A', bold: true, alignment: 'center' },
      { text: 'T', bold: true, alignment: 'center' },
    ]);
    summary.courses.forEach((item) => {
      const element = [];
      element.push({
        text: this.truncate(item.course.name).toUpperCase(),
        noWrap: true,
      });
      element.push(
        {
          text: '',
          alignment: 'center',
          color: 'white',
        },
        {
          text: '',
          alignment: 'center',
          color: 'white',
        },
        {
          text: '',
          alignment: 'center',
          color: 'white',
        },
        {
          text: '',
          alignment: 'center',
          bold: true,
          color: 'white',
        },
        { text: '' },
        { text: '' },
        { text: '' },
        { text: '' },
        { text: '' },
        { text: '' },
        { text: '' },
        { text: '' }
      );
      array.push(element);
      if (item.children) {
        item.children.forEach((child) => {
          const el = [];
          el.push({
            text: this.truncate(child.course.name).toUpperCase(),
            margin: [10, 0],
            noWrap: true,
          });
          el.push(
            {
              text: '',
              alignment: 'center',
              color: 'white',
            },
            {
              text: '',
              alignment: 'center',
              color: 'white',
            },
            {
              text: '',
              alignment: 'center',
              color: 'white',
            },
            { text: '' },
            { text: '' },
            { text: '' },
            { text: '' },
            { text: '' },
            { text: '' },
            { text: '' },
            { text: '' },
            { text: '' }
          );
          array.push(el);
        });
      }
    });
    const total = [];
    const averages: string[] = [];
    averages.push(
      this.avg(
        summary.courses
          .filter((x) => x.grades[0]?.score > 0)
          .map((x) => x.grades[0]?.score)
      )
    );
    averages.push(
      this.avg(
        summary.courses
          .filter((x) => x.grades[1]?.score > 0)
          .map((x) => x.grades[1]?.score)
      )
    );
    averages.push(
      this.avg(
        summary.courses
          .filter((x) => x.grades[2]?.score > 0)
          .map((x) => x.grades[2]?.score)
      )
    );
    total.push({ text: 'PROMEDIO', bold: true });
    total.push({
      text: '',
      alignment: 'center',
      bold: true,
      color: 'white',
    });
    total.push({
      text: '',
      alignment: 'center',
      bold: true,
      color: 'white',
    });
    total.push({
      text: '',
      alignment: 'center',
      bold: true,
      color: 'white',
    });
    total.push({
      text: '',
      alignment: 'center',
      bold: true,
      color: 'white',
    });
    total.push(
      { text: '' },
      { text: '' },
      { text: '' },
      { text: '' },
      { text: '' },
      { text: '' },
      { text: '' },
      { text: '' }
    );
    array.push(total);

    return array;
  }

  avg(nums: number[]) {
    if (nums.length) {
      return (nums.reduce((a, b) => a + b, 0) / nums.length).toFixed(2);
    }
  }

  getSkills(skills: StudentSkill[]) {
    const array: any[][] = [];

    array.push([
      { text: 'HÁBITOS Y ACTITUDES', bold: true },
      { text: 'I TRIMESTRE', bold: true, alignment: 'center' },
      { text: 'II TRIMESTRE', bold: true, alignment: 'center' },
      { text: 'III TRIMESTRE', bold: true, alignment: 'center' },
    ]);
    skills.forEach((skill) => {
      const element = [];
      element.push({
        text: this.truncate(skill.skill.name).toUpperCase(),
      });
      skill.periods.forEach((period) => {
        element.push({
          text: '',
          alignment: 'center',
          color: 'white',
        });
      });
      array.push(element);
    });

    return array;
  }

  getAverages(numbers: string[]) {
    const values = numbers.filter((x) => !!x).map((x) => +x);
    return mean(values).toFixed(2);
  }

  truncate = (input: string) =>
    input.length > 40 ? `${input.substring(0, 40)}...` : input;
}
