import { Injectable } from '@angular/core';
import { capitalize, trimStart } from 'lodash';
import * as _ from 'underscore';

@Injectable({ providedIn: 'root' })
export class UtilService {
  constructor() {}

  removeById(array: Array<any>, id: string): Array<any> {
    return array.filter((item) => item.id !== id);
  }

  groupBy = (data: any[], keyFn) =>
    data.reduce((agg, item) => {
      const group = keyFn(item);
      agg[group] = [...(agg[group] || []), { group, item }];
      return agg;
    }, {});

  updateItem(item: any, array: any[]): any[] {
    const objIndex = array.findIndex((obj) => obj.ID === item.ID);
    array[objIndex] = item;
    return array;
  }

  filterById(array: Array<any>, id: string, property: string = 'id'): any {
    return array.find((item) => item[property] === id);
  }

  isNullString(str?: string): string {
    return str || '';
  }

  getProperty(item: any, property: string): string {
    property?.split('.').forEach((e) => {
      item = item ? item[e] : '';
    });
    return item;
  }

  searchFilter(
    array: Array<any>,
    args: Array<string>,
    searchText: string
  ): Array<any> {
    const filterArray: Array<any> = [];

    searchText = searchText.toLocaleLowerCase();

    for (const item of array) {
      let term = '';
      for (const col of args) {
        term = term + this.isNullString(this.getProperty(item, col));
      }
      term = term.toLocaleLowerCase();
      if (term.indexOf(searchText) >= 0) {
        filterArray.push(item);
      }
    }
    return filterArray;
  }

  paginate(itemsCount: number, currentPage: number = 1, pageSize: number): any {
    const totalPages: number = Math.ceil(itemsCount / pageSize) + 1;
    let startPage: number;
    let endPage: number;

    if (totalPages <= 5) {
      startPage = 1;
      endPage = totalPages;
    } else {
      if (currentPage <= 3) {
        startPage = 1;
        endPage = 6;
      } else if (currentPage + 2 >= totalPages) {
        startPage = totalPages - 5;
        endPage = totalPages;
      } else {
        startPage = currentPage - 2;
        endPage = currentPage + 3;
      }
    }

    const startIndex = (currentPage - 1) * pageSize;
    const endIndex = Math.min(startIndex + (pageSize - 1), itemsCount - 1);

    const pages = _.range(startPage, endPage);

    return {
      totalItems: itemsCount,
      currentPage,
      pageSize,
      totalPages,
      startPage,
      endPage,
      startIndex,
      endIndex,
      pages,
    };
  }

  sortBy(array: Array<any>, args: string, desc?: boolean): Array<any> {
    if (desc) {
      array.sort((a: any, b: any) => {
        if (!a[args]) {
          return 1;
        } else if (!b[args]) {
          return -1;
        } else if (a[args] < b[args]) {
          return 1;
        } else if (a[args] > b[args]) {
          return -1;
        } else {
          return 0;
        }
      });
    } else {
      array.sort((a: any, b: any) => {
        if (!a[args]) {
          return -1;
        } else if (!b[args]) {
          return 1;
        } else if (a[args] < b[args]) {
          return -1;
        } else if (a[args] > b[args]) {
          return 1;
        } else {
          return 0;
        }
      });
    }
    return array;
  }

  titleCase = (input: string) => {
    let words = input.split(' ');
    words = words.map((x) => capitalize(x));
    let result = '';
    words.forEach((x) => {
      result = `${result} ${x}`;
    });
    return trimStart(result);
  };
}
