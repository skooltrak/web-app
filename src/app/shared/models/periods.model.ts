export interface Period {
  id: string;
  sort: number;
  name: string;
  startDate: Date;
  endDate: Date;
}
