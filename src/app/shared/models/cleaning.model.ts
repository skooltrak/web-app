export interface CleaningItem {
  code: string;
  description: string;
  value: number;
}
