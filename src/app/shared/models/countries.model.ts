export interface Country {
  id: string;
  name: string;
  provinces: string[];
}
