import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TippyModule } from '@ngneat/helipopper';
import { TranslocoModule } from '@ngneat/transloco';

import { AvatarPipe } from '../../pipes/avatar.pipe';
import { ProfilePopoverComponent } from './profile-popover.component';
import { LoadingModalComponent } from '../custom-components/loading-modal/loading-modal.component';

@NgModule({
  imports: [CommonModule, TippyModule, LoadingModalComponent, TranslocoModule],
  declarations: [ProfilePopoverComponent],
  exports: [ProfilePopoverComponent],
  providers: [AvatarPipe],
})
export class ProfilePopoverModule {}
