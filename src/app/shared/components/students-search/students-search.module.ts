import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslocoModule } from '@ngneat/transloco';

import { StudentsSearchComponent } from './students-search.component';
import { CustomTableComponent } from '../custom-components/custom-table/custom-table.component';

@NgModule({
  declarations: [StudentsSearchComponent],
  imports: [
    CommonModule,
    NgbModalModule,
    TranslocoModule,
    CustomTableComponent,
  ],
  exports: [StudentsSearchComponent],
})
export class StudentsSearchModule {}
