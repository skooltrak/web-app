import { Component, Input, OnInit } from '@angular/core';
import {
  FormControl,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { Column } from '../custom-table/table-options';
import { UtilService } from 'src/app/shared/services/util.service';
import { RegexEnum } from 'src/app/shared/enums/regex.enum';
import { CustomDatepickerComponent } from '../custom-datepicker/custom-datepicker.component';
import { TranslocoModule } from '@ngneat/transloco';
import { CommonModule } from '@angular/common';
import { CustomSelectComponent } from '../custom-select/custom-select.component';

@Component({
  selector: 'skooltrak-custom-form',
  standalone: true,
  imports: [
    FormsModule,
    CustomDatepickerComponent,
    TranslocoModule,
    ReactiveFormsModule,
    CommonModule,
    CustomSelectComponent,
  ],
  templateUrl: './custom-form.component.html',
  styleUrls: ['./custom-form.component.sass'],
})
export class CustomFormComponent implements OnInit {
  @Input() fields!: Column[];
  @Input() item?: any = {};

  form: FormGroup = new FormGroup({});

  constructor(
    public readonly activeModal: NgbActiveModal,
    private readonly util: UtilService
  ) {}

  ngOnInit(): void {
    this.resolveLists();
    this.generateForm();
  }

  generateForm(): void {
    this.fields
      .filter((x) => !x.readonly)
      .forEach((field) => {
        const control = new FormControl(this.item[field.name]);
        const validators: ValidatorFn[] = [];

        if (field.required) {
          validators.push(Validators.required);
        }

        if (field.type === 'email') {
          validators.push(Validators.pattern(RegexEnum.EMAIL));
        }

        if (field.type === 'mobile-phone') {
          validators.push(Validators.pattern(RegexEnum.MOBILE_PHONE));
        }

        if (field.type === 'home-phone') {
          validators.push(Validators.pattern(RegexEnum.HOME_PHONE));
        }

        if (validators.length) {
          control.setValidators(validators);
        }
        this.form.addControl(field.name, control);
      });
  }

  updateObject(event: any): void {
    this.fields
      .filter((x) => !x.readonly)
      .forEach((field) => {
        if (field.type === 'object' && this.item[field.objectID!]) {
          this.item[field.name] = this.util.filterById(
            field.list!,
            this.item[field.objectID!]
          );
          this.item[field.objectText!] =
            this.item[field.name][field.listDisplay];
        } else {
          this.item[field.name] = this.form.get(field.name)?.value;
        }
      });
  }

  setFile(file: any, field: Column): void {
    this.item[field.name] = file.target.files;
  }

  resolveLists(): void {
    this.fields
      .filter((x) => !x.readonly && x.asyncList)
      .forEach((field) => {
        field.asyncList!.subscribe({
          next: (data) => {
            field.list = field.listDisplay
              ? this.util.sortBy(data, field.listDisplay)
              : this.util.sortBy(data, 'name');
          },
          error: (err) => {
            console.error(err);
          },
        });
      });
  }

  validateForm(): void {
    if (this.form.valid) {
      this.activeModal.close();
    }
  }

  compareFn(c1: any, c2: any): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }
}
