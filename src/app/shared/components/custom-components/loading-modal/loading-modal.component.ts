import { Component } from '@angular/core';
import { AnimationOptions, LottieModule } from 'ngx-lottie';

@Component({
  selector: 'skooltrak-loading-modal',
  templateUrl: './loading-modal.component.html',
  standalone: true,
  imports: [LottieModule],
  styleUrls: ['./loading-modal.component.sass'],
})
export class LoadingModalComponent {
  options: AnimationOptions = {
    path: '/assets/animations/loading-books.json',
  };
}
