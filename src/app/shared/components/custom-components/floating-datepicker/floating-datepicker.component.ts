import { Component, forwardRef, Input } from '@angular/core';
import {
  ControlValueAccessor,
  FormsModule,
  NG_VALUE_ACCESSOR,
} from '@angular/forms';
import {
  NgbDateParserFormatter,
  NgbDatepickerModule,
  NgbDateStruct,
} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'skooltrak-floating-datepicker',
  templateUrl: './floating-datepicker.component.html',
  standalone: true,
  imports: [NgbDatepickerModule, FormsModule],
  styleUrls: ['./floating-datepicker.component.sass'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => FloatingDatepickerComponent),
      multi: true,
    },
  ],
})
export class FloatingDatepickerComponent implements ControlValueAccessor {
  @Input() label!: string;
  @Input() id!: string;

  @Input() maxDate: NgbDateStruct = {
    year: new Date().getFullYear() - 5,
    month: 1,
    day: 1,
  };
  @Input() minDate: NgbDateStruct = {
    year: new Date().getFullYear(),
    month: 12,
    day: 31,
  };
  @Input() required: boolean = false;
  current!: NgbDateStruct | null;
  disabled: boolean = false;
  constructor(private formatter: NgbDateParserFormatter) {}

  get value(): Date {
    return new Date(this.formatter.format(this.current));
  }

  onChange = (date: any): void => {};

  change(): void {
    this.onChange(this.value);
  }

  onTouched = (): void => {};

  writeValue(date: Date): void {
    if (date) {
      this.current = this.formatter.parse(date.toString());
      this.onChange(this.value);
    }
  }

  registerOnChange(fn: () => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }
}
