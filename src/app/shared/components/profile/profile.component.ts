import { Component, OnInit } from '@angular/core';
import { UntypedFormArray, UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { TranslocoService } from '@ngneat/transloco';
import { combineLatest, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import Swal from 'sweetalert2';

import { User } from '../../models/users.model';
import { FilesService } from '../../services/files.service';
import { SessionService } from '../../services/session.service';
import { UsersService } from '../../services/users.service';

@Component({
  selector: 'skooltrak-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.sass'],
})
export class ProfileComponent implements OnInit {
  profile: User;
  profileForm: UntypedFormGroup;
  constructor(
    public session: SessionService,
    private fb: UntypedFormBuilder,
    private user: UsersService,
    private transloco: TranslocoService,
    private filesService: FilesService
  ) {}

  ngOnInit(): void {
    this.profile = this.session.currentUser;
    this.profileForm = this.fb.group({
      userName: [this.profile.userName, [Validators.required]],
      displayName: [this.profile.displayName, [Validators.required]],
      email: [this.profile.email, [Validators.required, Validators.email]],
      notificationMails: this.profile.notificationMails
        ? this.fb.array(this.initEmails())
        : this.fb.array([]),
    });
  }

  changeAvatar(event: any): void {
    event.preventDefault();
    const element: HTMLElement = document.getElementById('avatar');
    element.click();
  }

  get emails(): UntypedFormArray {
    return this.profileForm.get('notificationMails') as UntypedFormArray;
  }

  addEmail(): void {
    const controls = this.profileForm.get('notificationMails') as UntypedFormArray;
    controls.push(new UntypedFormControl('', [Validators.email]));
  }

  initEmail(email?: string): UntypedFormControl {
    return this.fb.control(email, [Validators.required, Validators.email]);
  }

  initEmails(): UntypedFormControl[] {
    const controls: UntypedFormControl[] = [];
    if (!this.profile.notificationMails) {
      this.profile.notificationMails = [];
    }
    this.profile.notificationMails.forEach((mail) => {
      controls.push(this.initEmail(mail));
    });
    return controls;
  }

  removeEmail(i: number): void {
    const controls = this.profileForm.controls.notificationMails as UntypedFormArray;
    controls.removeAt(i);
  }

  updateProfile(): void {
    this.user.updateInfo(this.profile.id, this.profileForm.value).subscribe({
      next: () => {
        this.session.currentUser.displayName =
          this.profileForm.value.displayName;
        this.session.currentUser.email = this.profileForm.value.email;
        this.session.currentUser.userName = this.profileForm.value.userName;
        this.session.currentUser.notificationMails =
          this.profileForm.value.notificationMails;
        Swal.fire(
          '',
          this.transloco.translate('Updated item', {
            value: this.transloco.translate('Profile'),
          }),
          'success'
        );
      },
      error: (err: Error) => {
        Swal.fire(
          this.transloco.translate('Something went wrong'),
          err.message,
          'error'
        );
      },
    });
  }

  setAvatar(file: any): void {
    this.filesService
      .uploadFile(file)
      .pipe(
        mergeMap((resp) =>
          combineLatest([
            this.user.changeAvatar(this.session.currentUser.id, resp.id),
            of(resp),
          ])
        )
      )
      .subscribe({
        next: ([value, resp]) => {
          this.session.currentUser.photoURL = resp.id;
          Swal.fire(
            this.transloco.translate('Profile picture updated'),
            '',
            'success'
          );
        },
        error: (err: Error) => {
          Swal.fire(
            this.transloco.translate('Something went wrong'),
            err.message,
            'error'
          );
        },
      });
  }
}
