import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslocoModule } from '@ngneat/transloco';

import { SharedModule } from '../../shared.module';
import { ModalPlayerComponent } from './modal-player/modal-player.component';
import { UploaderComponent } from './uploader/uploader.component';
import { VideoPlayerComponent } from './video-player.component';
import { CustomSelectComponent } from '../custom-components/custom-select/custom-select.component';

@NgModule({
  declarations: [VideoPlayerComponent, UploaderComponent, ModalPlayerComponent],
  imports: [
    CommonModule,
    NgbModalModule,
    TranslocoModule,
    ReactiveFormsModule,
    CustomSelectComponent,
    FormsModule,
    SharedModule,
  ],
  exports: [VideoPlayerComponent, UploaderComponent, ModalPlayerComponent],
})
export class VideoPlayerModule {}
