import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModalModule, NgbNavModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslocoModule } from '@ngneat/transloco';
import { NgxSummernoteModule } from 'ngx-summernote';

import { CourseGradesModule } from '../course-grades/course-grades.module';
import { CustomDatepickerComponent } from '../custom-components/custom-datepicker/custom-datepicker.component';
import { EditorjsModule } from '../editorjs/editorjs.module';
import { AssignmentFormComponent } from './assignment-form.component';

@NgModule({
  declarations: [AssignmentFormComponent],
  exports: [AssignmentFormComponent],
  imports: [
    CommonModule,
    TranslocoModule,
    NgbModalModule,
    EditorjsModule,
    NgxSummernoteModule,
    FormsModule,
    ReactiveFormsModule,
    CustomDatepickerComponent,
    NgbNavModule,
    CourseGradesModule,
  ],
})
export class AssignmentFormModule {}
